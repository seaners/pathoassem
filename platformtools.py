__author__ = 'scorbett'

# A file to determine the platform PathoAssem is running on, and locate binaries for different assemblers.

import sys
import subprocess
import re


def locate_binary(assembler):
    #TODO Subclasses for different platforms?
    if re.match('linux|darwin', sys.platform):
        try:
            executable_path = subprocess.check_output(["which", assembler.assembler_name])
            return executable_path.rstrip()

        #TODO: Check out other potential errors we might run into.
        except subprocess.CalledProcessError:
            #Presumably, can't find executable in the path
            #TODO: Option to specify assembler location..
            raise RuntimeError("ERROR: Could not locate executable for assembler '%s'", assembler.assembler_name)

    elif sys.platform == 'win32':
        #TODO Windows support
        raise RuntimeError("ERROR: Windows support not yet implemented.")

    else:
        #TODO Other platforms
        raise RuntimeError("ERROR: Support for platform '%s' not yet implemented." % sys.platform)
