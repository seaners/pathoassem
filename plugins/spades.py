__author__ = "Demarcus Briers"

from plugins.base import AssemblerBase
import os
import platformtools


class Spades(AssemblerBase):

    #TODO: Need to determine seqType, memory, CPU, etc. based off user input
    #TODO: More elegant means for executable path substitution?
    #TODO: Detect Python binary path too?

    single_end_cmd = ""
    paired_end_cmd = "python %s --pe1-1  %s --pe1-2 %s -o %s"
    interlaced_cmd = ""

    def __init__(self, dna=False, rna=False, reads=None, left=None, right=None, reference=None, denovo=False, refguide=False, outdir=None):
        super(Spades, self,).__init__(dna, rna, reads, left, right, reference, denovo, refguide, outdir)
        self.pair_end_assembly = False
        self.assembler_name = "spades.py"

        #TODO: More elegant system for defining binary path...?
        self.executable_path = platformtools.locate_binary(self)

    def verify_sufficient_params(self, assembly_params):
        #TODO: Implement exceptions for insufficient params and handle in top level module
        #TODO: Determine sufficient verification steps..
        #TODO: Need more subtle way to tell how assembly went based off SPAdes exit status
        if assembly_params.left and assembly_params.right:
            return True

        elif assembly_params.reads:
            return True
        else:
            return False

    def perform_assembly(self, assembly_params):
        if assembly_params.refguide:
            raise NotImplementedError("ERROR: PathoAssem doesn't support reference based SPAdes assembly.")

        #TODO Single end reads
        #TODO Pair end interlaced reads
        status = os.system(self.paired_end_cmd % (self.executable_path, self.left, self.right, self.outdir))

        return status
