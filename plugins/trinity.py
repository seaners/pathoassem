__author__ = "Sean Corbett"

from plugins.base import AssemblerBase
import os
import platformtools


#TODO    - Obviously need to locate Trinity binary and make sure it's executable;
#TODO           maybe interactively?


class Trinity(AssemblerBase):

    #TODO: Need to determine seqType, memory, CPU, etc. based off user input?
    #TODO: More elegant means for executable path substitution?
    single_end_cmd = "%s --seqType fq --max_memory 4G --CPU 2 --single %s"
    pair_end_cmd = "%s --seqType fq --max_memory 4G --CPU 2 --left %s --right %s"

    def __init__(self, dna=False, rna=False, reads=None, left=None, right=None, denovo=False, refguide=False, outdir=None):
        super(Trinity, self,).__init__(dna, rna, reads, left, right, denovo, refguide, outdir)
        self.pair_end_assembly = False
        self.assembler_name = "Trinity"

        #TODO: More elegant system for defining binary path...?
        self.executable_path = platformtools.locate_binary(self)

    def verify_sufficient_params(self, assembly_params):
        #TODO: Implement exceptions for insufficient params and handle in top level module
        #TODO: Determine sufficient verification steps..
        if assembly_params.left and assembly_params.right:
            return True
        elif assembly_params.reads:
            return True
        else:
            return False

    #TODO: Need more subtle way to tell how assembly went based off Trinity exit status
        return True

    def perform_assembly(self, assembly_params):
        if assembly_params.refguide:
            raise NotImplementedError("ERROR: PathoAssem doesn't support reference based transcriptome assembly.")

        #TODO Paired versus single end reads
        status = os.system(self.single_end_cmd % (self.executable_path, self.reads))
        return status
