import abc
import six

#TODO: Passthrough mechanism to supply params directly to assemblers?

@six.add_metaclass(abc.ABCMeta)
class AssemblerBase(object):
    """
    Base class for plugins which wrap arbitrary genome/trasncriptiome assemblers
    """

    def __init__(self, dna=False, rna=False, reads=None, left=None, right=None, reference=None,denovo=False, refguide=False, outdir=None):
        self.dna = dna
        self.rna = rna
        self.reads = reads
        self.left = left
        self.right = right
        self.reference = reference
        self.denovo = denovo
        self.refguide = refguide
        self.outdir = outdir

        self.assembler_name = ""
        self.executable_path = ""

    @abc.abstractmethod
    def verify_sufficient_params(self, assembly_params):
        """
        Make sure sufficient parameters were provided to be able to run
        the assembler the concrete plugin wraps.
        :param assembly_params: The command line args
        :type assembly_params: ArgumentParser
        :return: True if we have sufficient params, False otherwise
        """
        #TODO: use exception for insufficient params?

    @abc.abstractmethod
    def perform_assembly(self, assembly_params):
        """
        Run the assembler that the concrete plugin wraps.
        :param assembly_params The command line args
        :type assembly_params: ArgumentParser
        :returns True if assembly was successful, False otherwise
        """
