__author__ = "Demarcus Briers"

from plugins.spades import Spades
import os
import platformtools


#Note. We use a modified version of scaffold_builder that lets the user choose an output_folder an
# not just an output prefix.

class Scaffold(Spades):

    #TODO: Need to determine seqType, memory, CPU, etc. based off user input
    #TODO: More elegant means for executable path substitution?
    #TODO: Modify scaffold_builder code to output to absolute folder path and not relative to working directory`
    contigs_cmd = "%s -q %s -r %s -p %s"

    def __init__(self, dna=False, rna=False, reads=None, left=None, right=None, reference=None, denovo=False, refguide=False, outdir=None):
        super(Spades, self,).__init__(dna, rna, reads, left, right, reference, denovo, refguide, outdir)
        self.pair_end_assembly = False
        self.assembler_name = "scaffold_builder.py"

        #TODO: More elegant system for defining binary path...?
        self.executable_path = platformtools.locate_binary(self)

    def verify_sufficient_params(self, assembly_params):
        #TODO: Implement exceptions for insufficient params and handle in top level module
        #TODO: Determine sufficient verification steps.
        #Requirements for scaffolding 1.) contigs  2.)reference genome.
        #TODO: How to check if reference genome is provided.
        #TODO: Should there be a diff param for contigs vs reads
        if assembly_params.reads and assembly_params.refguide:
            return True

        elif assembly_params.reads:
            return False

        #TODO: Need more subtle way to tell how assembly went based off Scaffold Builder exit status
        return True

    def perform_assembly(self, assembly_params):
        if assembly_params.left or assembly_params.right:
            raise NotImplementedError("ERROR: PathoAssem reference based scaffolding needs contigs in FASTA format. Not paired-end reads!")

        status = os.system(self.contigs_cmd % (self.executable_path, self.reads, self.reference, self.outdir))

        return status
