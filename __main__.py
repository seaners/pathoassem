__author__ = "Sean Corbett"

import argparse
from stevedore import driver
import sys
import os
from . import platformtools


#TODO: Help function to print all arguments
#TODO: Option to detect and print all available plugins
def main():
    args = get_args()
    manager = resolve_assembler(args)

    #TODO: Better abstraction around performing assembly
    #TODO: Call method for locating executable
    manager.driver.perform_assembly(args)


#TODO Messages file for all these strings
def get_args():
    args = None

    parser = argparse.ArgumentParser()
    parser.add_argument("--dna", dest="dna", action="store_true", help="Perform an assembly with genome data")
    parser.add_argument("--rna", dest="rna", action="store_true", help="Perform an assembly with transcriptome data")

    parser.add_argument("--reads", dest="reads", help="Location of read file for assembling single end sequence data")
    parser.add_argument("--left", dest="left", help="Location of left read file when assembling paired end sequence data")
    parser.add_argument("--right", dest="right", help="Location of right read file when assembling paired end sequence data")
    parser.add_argument("--reference", dest="reference", help="Location of reference genome(fasta) when performing a reference-guided assembly")
    
    parser.add_argument("--denovo", dest="denovo", action="store_true", help="Perform a de novo genome or transcriptome assembly")
    parser.add_argument("--refguide", dest="refguide", action="store_true", help="Perform a reference guided gnome or transcriptome assembly")

    parser.add_argument("--outdir", dest="outdir", help="Location of output direct ")

    parser.set_defaults(dna=False, rna=False, reads=False, left=False, right=False, denovo=False, refguide=False, reference=None, outdir='./')

    args = parser.parse_args()
    validate_args(args, parser)
    return args


def validate_args(args, parser):
    #TODO: More robust checks for minimum sufficient params
    #TODO: Param grouping in help message, take out __main__.py
    if not (args.dna or args.rna):
        parser.print_help()
        sys.exit()

    #TODO create directory if it doesn't exist
    #Make sure the output directory is good to go
    if not os.access(args.outdir, os.W_OK & os.X_OK):
        sys.exit("ERROR: Specified output directory doesn't exist, or has improper permissions")

    if args.dna and args.rna:
        sys.exit("ERROR: Please specify only one of --dna or --rna")

    if args.reads and (args.left or args.right) or (args.left and not args.right) or (args.right and not args.left):
        sys.exit("ERROR: Please specify either --reads, or both --forwardreads and --reversereads")

    if args.denovo and args.refguide:
        sys.exit("ERROR: Please specify only one of --denovo or --refguide")


def resolve_assembler(args):
    plugin_to_load = ""
    #TODO Get all plugins

    #TODO *general* mechanism for deciding which to use...
    #if args.reads and args.rna and args.denovo:
    if args.dna:
        if args.denovo:
            plugin_to_load = "spades"
        elif args.refguide:
            plugin_to_load = "refguided"
    elif args.rna:
        plugin_to_load = "trinity"


    #Load desired plugin
    mgr = driver.DriverManager(
        namespace="pathoassem.assembler",
        name=plugin_to_load,
        invoke_on_load=True,
        invoke_kwds=vars(args)
    )
    return mgr


if __name__ == "__main__":
    print("Running PathoAssem")
    main()
