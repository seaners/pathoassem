__author__ = 'Sean Corbett'

from setuptools import setup, find_packages

#TODO: Review all fields before publishing to Github / PyPI
#TODO: Need to add all plugins as they're written!
setup(
    name='pathoassem',
    version='0.0.1',
    description='PathoAssem: a modular application for metagenomic/transcriptomic assemblies.',
    platforms = ['Any'],

	install_requires=[
        "stevedore",
    ],

    provides=['pathoassem'],
    packages = find_packages(),
    include_package_data = True,
    entry_points = {
        'pathoassem.assembler': [
            'trinity = plugins.trinity:Trinity',
            'spades = plugins.spades:Spades',
            'refguided = plugins.refguided:Refguided'
        ],
    },
    zip_safe=False
)
