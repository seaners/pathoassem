### Johnson Lab, Boston University
### Modified by Demarcus Briers, Sean Corbett

import sys
import linecache

#tis=sys.argv[2:]
#samfile=open(sys.argv[1])
#out=open(sys.argv[1][:-4]+'.extracted.'+".".join(tis)+'.sam','w',0)

def extract_reads(tis, pathoscope_sam, pathoscope_tsv, OUTPUT_PREFIX):
""" 
Extracts reads from the Pathoscope output file based on their Taxonomy ID (ti)
If no reads are specified, identifies the top identified species in the Pathoscope
output file and extracts reads for that species' tline.

TODO: Need to check for file existence and readability.

PARAMS:
tis -> An array of taxonomy IDs to extract from the pathoscope output
pathoscope_sam -> the path to the Pathoscope output SAM file.
pathoscope_tsv -> the path to the Pathoscope output TSV with species attributions.
OUTPUT_PREFIX -> the path to the output directory specified by the user.

RETURNS:
If there was an error TODO uhm?
Path to the extracted read file, if everything went ok.
"""

    #TODO: look for best practices for file opening -- this is pretty rudimentary.
    #TODO: What if SAM is really big? Does iterator below load the whole thing at once?
    samfile = open(pathoscope_sam, 'r')

    extracted_reads_filename = pathoscope_sam[:-4] + ".extracted." + ".".join(tis) + ".sam", 'w', 0)
    readsfile = open(OUTPUT_PREFIX + "/extracted_reads/" + extracted_reads_filename) #TODO Careful with slashes!!

    if len(tis) == 0:
        tis.append(extract_ti_from_tsv(pathoscope_tsv))

    
    ### Check for a properly formatted SAM/BAM file.
    # Valid SAM/BAM file only has one @HD line.
    hasHDheader=False
    # Valid SAM/BAM @PG lines must have a unique id field.
    usedPGids=[]

    for line in samfile:
        tmp = line.split()
        if tmp[0] == "@HD":
            if hasHDheader == False:
                hasHDheader = True
                print(line, file=readsfile)
            continue
        elif tmp[0]=="@PG":
            if tmp[1] not in usedPGids:
                usedPGids.append(tmp[1])
                print(line, file=readsfile)
            continue

        #ti might be in second or third position on these type lines.            
        #TODO: Make sure that this is actually a possibility, otherwise remove.
        if len(tmp[1].split("|")) > 1:
            if tmp[1].split("|")[1] in tis:
                print(line, file=readsfile)
                continue 
        if len(tmp[2].split("|")) > 1:
            if tmp[2].split("|")[1] in tis:
                print(line, file=readsfile)

    readsfile.close()
    samfile.close()
    return extracted_reads_filename


#Check out the standard Pathoscope output TSV to see 
def extract_ti_from_tsv(pathoscope_tsv):
    top_hit_line = linecache.getline(pathoscope_tsv, 3)
    ti = top_hit_line.split("\t")[0].split("|")[1]
    return ti
